@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        Como conectar ao {{ Config::get('server.sv.realmd.name') }}
                    </h3>
                    <div class="byline">
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Recuperação de Senha" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <div class="content-features">

                        <div class="column-left popular pvp-summary">
                            <h3 class="category" style="margin-top: 30px;">Passo 1: Conta do Jogo</h3>
                            <div class="top-bgs" style="line-height: 2em;">
                                <p>Para entrar no nosso servidor você precisa de uma conta do jogo. Há um grande botão no canto direito da página que diz "<a>Criar Conta</a>".</p>

                                <p>No caso de você não conseguir encontrá-lo, você pode acessá-lo <a href="/user/register" target="_blank">AQUI</a>.</p>

                                <p>Ao criar a conta, tenha em mente que você precisa fornecer um endereço de e-mail válido. Este endereço de e-mail é a única maneira de recuperar a senha, se caso esquecê-la.</p>

                                <p>Após criar sua conta, você receberá um e-mail com um link de ativação que você precisa clicar para ter a conta ativada.</p>

                                <p>Escolha o nome da conta e senha que são seguros. Evite contas que terão o mesmo nome de seus personagens ou senhas como 123456. Nós não assumiremos a responsabilidade pela segurança de sua conta.</p>
                            </div>
                        </div>

                        <div class="column-left popular pvp-summary">
                            <h3 class="category" style="margin-top: 30px;">Passo 2: Baixando o Jogo</h3>
                            <div class="top-bgs" style="line-height: 2em;">
                                <p>Você precisa ter o jogo instalado. Se você já tem o jogo instalado, você pode pular o passo 2.</p>

                                <p>Você pode baixar o jogo The Burning Crusade via torrent <a href="http://tinyurl.com/lpau68s" target="_blank">clicando aqui</a>.</p>

                                <p>Nota: Você pode baixá-lo a partir de qualquer fonte que você queira, mas certifique-se de confiar na fonte.</p>
                            </div>
                        </div>

                        <div class="column-left popular pvp-summary">
                            <h3 class="category" style="margin-top: 30px;">Passo 3: Alterando o Realmlist</h3>
                            <div class="top-bgs" style="line-height: 2em;">
                                <p>Para ser capaz de se conectar ao nosso servidor você precisa alterar o arquivo o conteúdo do arquivo <a>realmlist.wtf</a> para apontar para {{ Config::get('server.sv.realmd.name') }}.</p>

                                <p>Você pode encontrar este arquivo dentro da pasta base da instalação do jogo (o mesmo diretório que está o arquivo WoW.exe).</p>

                                <p>Abra o arquivo <a>realmlist.wtf</a> com bloco de notas, apague tudo que está dentro dele e escreva:</p>
                                <p style="font-size:16px;"><a>set realmlist {{ Config::get('server.sv.realmd.url') }}</a></p>
                                <p>Salve o arquivo e feche-o.</p>

                            </div>
                        </div>

                        <div class="column-left popular pvp-summary">
                            <h3 class="category" style="margin-top: 30px;">Passo 4: Entrando no Jogo</h3>
                            <div class="top-bgs" style="line-height: 2em;">
                                <p>Agora tudo está configurado e você só tem que entrar dentro. Mas certifique-se:</p>

                                <ul>
                                    <li>Abra o jogo pelo arquivo <a>WoW.exe</a>. Nunca use Launcher.exe.</li>
                                    <li>Logue com o nome de usuário que você escolheu quando criou a conta aqui. Não tente usar o endereço de e-mail para esta finalidade.</li>
                                </ul>
                            </div>
                        </div>


                        <div class="column-left popular pvp-summary">
                            <h3 class="category" style="margin-top: 30px;">Possíveis problemas ao entrar no Jogo</h3>
                            <div class="top-bgs" style="line-height: 2em;">
                                <p style="font-size:16px;"><a>Ao abrir o jogo ele tenta atualizar</a></p>
                                <ul>
                                    <li>Certifique-se de que você está usando <a>wow.exe</a> para executar o jogo. Se você estiver usando o atalho criado automaticamente em seu desktop, certifique-se que o mesmo aponta para wow.exe (por padrão ele não faz).</li>
                                    <li>Verifique o arquivo <a>realmlist.wtf</a>. Veja o <a>passo 3</a> novamente.</li>
                                </ul>

                                <p style="font-size:16px;margin-top: 20px;"><a>Ao logar no jogo ele tenta atualizar</a></p>
                                <ul>
                                    <li>Não use o endereço de e-mail para efetuar login no servidor. Veja o <a>passo 5</a> novamente.</li>
                                </ul>

                                <p style="font-size:16px;margin-top: 20px;"><a>Recebi a mensagem "<b>Unable to validate game version</b>"</a></p>
                                <ul>
                                    <li>Verifique a versão do patch do seu jogo. Veja o <a>passo 2</a> novamente.</li>
                                    <li>Verifique a versão do patch do seu jogo. Veja o <a>passo 2</a> novamente.</li>
                                </ul>

                                <p style="font-size:16px;margin-top: 20px;"><a>Recebi a mensagem "<b>The information you have entered is not valid</b>"</a></p>
                                <ul>
                                    <li>Tente digitar as informações novamente. Certifique-se de que você não errou na digitação e sua tecla caps lock está desligado.</li>
                                    <li>Confira o seu arquivo realmlist, certifique-se que realmente está apontando para <a>{{ Config::get('server.sv.realmd.name') }}</a>. Veja o <a>passo 3</a> novamente.</li>
                                    <li>No caso de você não ser um jogador novo, quando foi a última vez que você usou a sua conta? Nós apagamos todas as contas inativas após 90 dias.</li>
                                </ul>

                                <p style="font-size:16px;margin-top: 20px;"><a>Recebi a mensagem "<b>verificar</b>"</a></p>
                                <ul>
                                    <li>Sua conta não foi ativada. Verifique o link de ativação da conta que enviamos ao seu endereço de email.</li>
                                </ul>
                            </div>
                        </div>



                        <span class="clear" style="height: 100px"><!-- --></span>

                    </div>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection