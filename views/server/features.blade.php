@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        {{ Config::get('server.sv.realmd.name') }} - The Burning Crusade
                    </h3>
                    <div class="byline">
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Recuperação de Senha" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <div class="content-features">

                        <div class="column-left popular pvp-summary">
                            <h3 class="category ">Realm</h3>
                            <div class="top-bgs">
                                <div class="table ">
                                    <table>
                                        <tbody>

                                        <tr class="row1">
                                            <td>XP Rate</td>
                                            <td><span class="rating-online">1x</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>Gold, Drop Rate</td>
                                            <td><span class="rating-online">1x</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>Final de Semana (sábado e domingo)</td>
                                            <td><span class="rating-online">Rate: Honor 2x, Rep 2x</span></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="column-left popular pvp-summary">
                            <h3 class="category ">Battlegrounds & Arenas</h3>
                            <div class="top-bgs">
                                <div class="table ">
                                    <table>
                                        <tbody>

                                        <tr class="row1">
                                            <td>Warsong Gulch</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>Arathi Basin</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>Eye of the Storm</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>Alterac Valley</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>Blade’s Edge Arena</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>Nagrand Arena</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>Ruins of Lordaeron</td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="column-left popular pvp-summary">
                                <h3 class="category ">Dungeons</h3>
                                <div class="top-bgs">
                                    <div class="table ">
                                        <table>
                                        <tbody>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3790" target="_blank" class="color-c6">
                                                    Auchindoun: Auchenai Crypts
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3792" target="_blank" class="color-c6">
                                                    Auchindoun: Mana-Tombs
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3791" target="_blank" class="color-c6">
                                                    Auchindoun: Sethekk Halls
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3789" target="_blank" class="color-c6">
                                                    Auchindoun: Shadow Labyrinth
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=2367" target="_blank" class="color-c6">
                                                    Caverns of Time: Old Hillsbrad Foothills
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=2366" target="_blank" class="color-c6">
                                                    Caverns of Time: The Black Morass
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3717" target="_blank" class="color-c6">
                                                    Coilfang Reservoir: The Slave Pens
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3715" target="_blank" class="color-c6">
                                                    Coilfang Reservoir: The Steamvault
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3716" target="_blank" class="color-c6">
                                                    Coilfang Reservoir: The Underbog
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3562" target="_blank" class="color-c6">
                                                    Hellfire Citadel: Hellfire Ramparts
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3713" target="_blank" class="color-c6">
                                                    Hellfire Citadel: The Blood Furnace
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3714" target="_blank" class="color-c6">
                                                    Hellfire Citadel: The Shattered Halls
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=4131" target="_blank" class="color-c6">
                                                    Magisters&#8217; Terrace
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3848" target="_blank" class="color-c6">
                                                    Tempest Keep: The Arcatraz
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3847" target="_blank" class="color-c6">
                                                    Tempest Keep: The Botanica
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3849" target="_blank" class="color-c6">
                                                    Tempest Keep: The Mechanar
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>


                                        </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <div class="column-left popular pvp-summary">
                            <h3 class="category ">Raids</h3>
                            <div class="top-bgs">
                                <div class="table ">
                                    <table>
                                        <tbody>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3457" target="_blank" class="color-c6">
                                                    Karazhan
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3923" target="_blank" class="color-c6">
                                                    Gruul&#8217;s Lair
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3836" target="_blank" class="color-c6">
                                                    Hellfire Citadel: Magtheridon&#8217;s Lair
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3607" target="_blank" class="color-c6">
                                                    Coilfang Reservoir: Serpentshrine Cavern
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3845" target="_blank" class="color-c6">
                                                    Tempest Keep: The Eye
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3606" target="_blank" class="color-c6">
                                                    Caverns of Time: Hyjal Summit
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowwiki.com/Zul'Aman_(original)" target="_blank" class="color-c6">
                                                    Zul&#8217;Aman
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-online">disponível</span></td>
                                        </tr>

                                        <tr class="row2">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=3959" target="_blank" class="color-c6">
                                                    Black Temple
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-offline">indisponível</span></td>
                                        </tr>

                                        <tr class="row1">
                                            <td>
                                                <a href="http://www.wowhead.com/zone=4075" target="_blank" class="color-c6">
                                                    Sunwell Plateau
                                                </a>
                                                <img src="/images/icons/bc_icon.gif" />
                                            </td>
                                            <td><span class="rating-offline">indisponível</span></td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <span class="clear"><!-- --></span>

                    </div>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection