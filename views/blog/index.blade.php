@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/homepage.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('conteudo')
<div id="homepage">
<div id="left">
    <div id="slideshow" class="ui-slideshow">
        <div class="slideshow">

            <div class="slide" id="slide-0" style="background-image: url(images/slideshow/HEPWPF7A2WZJ1369178148502.jpg); display: none;">

            </div>

            <div class="slide" id="slide-1" style="background-image: url(images/slideshow/WDP8P9O4JQFR1369927935258.jpg); display: block; opacity: 0.7004262222222223;">

            </div>
        </div>

        <div class="paging">

            <a href="javascript:;" id="paging-0" onclick="Slideshow.jump(0, this);" onmouseover="Slideshow.preview(0);" class="">
            </a>

            <a href="javascript:;" id="paging-1" onclick="Slideshow.jump(1, this);" onmouseover="Slideshow.preview(1);" class="last-slide current">
            </a>
        </div>

        <div class="caption" style="display: block; opacity: 0.5339662222222222;"><h3><a href="" class="link">bla bla bla bla</a></h3></div>

        <div class="preview" style="top: 15px; display: none;"></div>
        <div class="mask"></div>
    </div>

    <div class="homepage-news-wrapper">

        <div id="news-updates">
            <div id="news-updates-inner">

                @foreach ($blogs as $blog)
                <div class="news-article">
                    <div class="news-article-inner">
                        <h3>
                            <a href="/blog/{{ $blog->id }}/{{ $blog->slug() }}"> {{ $blog->title }} </a>
                        </h3>
                        <div class="by-line">
                            por <a href="">{{ $blog->user->profile->nickname }}</a> em
                            <span class="spacer" /> {{ $blog->created_at->format('d \d\e M \d\e Y H:i') }}
                            <a href="/blog/{{ $blog->id }}/{{ $blog->slug() }}#comments" class="comments-link">
                                <span class="icon-speech-bubble" />
                                {{ $blog->reply->count() }}
                            </a>
                        </div>

                        <div class="article-left" style="background-image: url('{{ $blog->thumbnail }}');">
                            <a href=""><span class="thumb-frame" /></a>
                        </div>

                        <div class="article-right">
                            <div class="article-summary">
                                <p><span style="font-size: 12px; line-height: 24px;">{{ $blog->text }}</span></p>

                                <a href="/blog/{{ $blog->id }}/{{ $blog->slug() }}" class="more">
                                    Mais
                                    <span class="icon-chevron-right" />
                                </a>
                            </div>
                        </div>

                        <span class="clear"><!-- --></span>
                    </div>
                </div>
                @endforeach

                <div class="blog-paging">
                    <a class="ui-button button1 button1-next float-right " href="">
                        <span>
                            <span>Próxima</span>
                        </span>
                    </a>
                    <span class="clear"><!-- --></span>
                </div>

            </div>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>
</div>

@endsection