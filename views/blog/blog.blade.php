@extends('template')

@section('page_title')
{{ $blog->title }} - {{ Config::get('server.site.title') }}
@endsection

@section('body_class')blog-page @endsection

@section('styles')
{{ HTML::style('/css/comments.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('scripts')
{{ HTML::script('/js/blog.js') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">

            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        {{ $blog->title }}
                    </h3>
                    <div class="byline">
                        <div class="blog-info">
                            por <a href="">{{ $blog->user->profile->nickname }}</a>
                            <span></span> {{ $blog->created_at->format('d \d\e M \d\e Y H:i') }}
                        </div>
                        <a class="comments-link" href="#comments">{{ $blog->reply->count() }}</a>
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="{{ $blog->title }}" src="{{ $blog->image }}" /></div>

                    <div class="detail">
                        <p style="font-size: 12px; line-height: 24px;">{{ $blog->text }}</p>
                    </div>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span id="comments"></span>

            <div id="page-comments">
                <div class="page-comment-interior">
                    <h3>Comentários ({{ $blog->reply->count() }})</h3>

                    @if (Auth::check() || ($blog->reply->count() > 0))
                    <div class="comments-container">

                        @if (Auth::check())
                        @if (Auth::user()->can('create_blog_reply') && $character)
                        <form action="/blog/reply/add" method="post" id="comment-form-reply" class="nested">
                            <fieldset>
                                <input type="hidden" id="reply_to" name="reply_to" value="" />
                                <input type="hidden" name="actual_page" value="{{ $_SERVER ['REQUEST_URI'] }}" />
                                <input type="hidden" name="blog_id" value="{{ $blog->id }}" />
                            </fieldset>

                            <div class="new-post">
                                <div class="comment">
                                    <div class="portrait-c">
                                        <div class="avatar-interior">
                                            <a href="javascript: void(0);">
                                                <img width="64" height="64" src="{{ $character->get_url_avatar() }}" alt="{{ $character->name }}" class="wow-class-{{ $character->class }}" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="comment-interior">

                                        <div class="character-info user">
                                            <div class="user-name">
                                                <a href="javascript: void(0);" class="context-link wow-class-{{ $character->class }}">{{ $character->name }}</a>
                                            </div>
                                        </div>

                                        <div class="content">
                                            <div class="comment-ta">
                                                <textarea id="comment-ta-reply" cols="78" rows="3" name="text"></textarea>
                                            </div>
                                            <div class="action">
                                                <div class="cancel">
                                                    <span class="spacer">|</span>
                                                    <a href="javascript:void(0);" onclick="$('#comment-form-reply').slideUp();">Cancelar</a>
                                                </div>
                                                <div class="submit">
                                                    <button class="ui-button button1 comment-submit " type="submit">
                                                        <span>
                                                            <span>Postar</span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <span class="clear"><!-- --></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="new-post">
                            <div class="comment">
                                <div class="portrait-b">
                                    <div class="avatar-interior">
                                        <a href="javascript: void(0);">
                                            <img width="64" height="64" src="{{ $character->get_url_avatar() }}" alt="{{ $character->name }}" class="wow-class-{{ $character->class }}" />
                                        </a>
                                    </div>
                                </div>
                                <form method="post" action="{{ URL::to('blog/reply/add') }}" id="add-reply">
                                    <input type="hidden" name="actual_page" value="{{ $_SERVER ['REQUEST_URI'] }}" />
                                    <input type="hidden" name="blog_id" value="{{ $blog->id }}" />

                                    <div class="comment-interior">
                                        <div class="character-info user">
                                            <div class="user-name">
                                                <a href="javascript: void(0);" class="context-link wow-class-{{ $character->class }}" rel="np">
                                                    {{ $character->name }}
                                                </a>
                                            </div>
                                        </div>
                                        <div class="content">
                                            <div class="comment-ta">
                                                <textarea id="comment-ta" cols="78" rows="3" name="text"></textarea>
                                            </div>
                                            <div class="action">
                                                <div class="submit">
                                                    <button class="ui-button button1 comment-submit " type="submit">
                                                        <span>
                                                            <span>Postar</span>
                                                        </span>
                                                    </button>
                                                </div>
                                                <span class="clear"><!-- --></span>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @else
                        <div class="comment">
                            <div class="comment-interior anonimo">
                                É necessário criar um perfil selecionando um de seus personagens em <a href="/dashboard">Minha Conta</a>
                                para que você possa escrever comentários.
                            </div>
                        </div>
                        @endif
                        @endif

                        @foreach ($replys as $reply)
                        <div class="comment" id="c-{{ $reply->id }}">

                            <div class="avatar portrait-b">
                                <div class="avatar-interior">
                                    <a href="javascript: void(0);">
                                        <img width="64" height="64" src="{{ $reply->character()->get_url_avatar() }}" alt="{{ $reply->character()->name }}" class="wow-class-{{ $reply->character()->class }}" />
                                    </a>
                                </div>
                            </div>

                            <div class="comment-interior">
                                <div class="character-info user">
                                    <div class="user-name">
                                        <a href="javascript: void(0);" class="context-link wow-class-{{ $reply->character()->class }}" rel="np">
                                            {{ $reply->character()->name }}
                                        </a>
                                    </div>

                                    <span class="time">
                                        <a href="#c-{{ $reply->id }}">{{ $reply->created_at->format('d \d\e M \d\e Y H:i') }}</a>
                                    </span>
                                </div>

                                <div class="content @if ($reply->user->level(3, '>='))wow-class-moderator@endif"><pre>{{ $reply->text }}</pre></div>

                                <div class="comment-actions">
                                @if (Auth::check())
                                    @if (Auth::user()->can('create_blog_reply') && $character)
                                        <a class="reply-link" href="javascript: void(0);" onclick="Blog.reply_add({{ $reply->id }}, {{ $reply->id }}, '{{ $reply->character()->name }}')">Responder</a>
                                    @endif
                                    @if (Auth::user()->can('delete_blog_reply') || $reply->user == Auth::user())
                                        <a href="javascript: void(0);" onclick="Blog.reply_delete({{ $reply->id }})">Excluir</a>
                                    @endif
                                @else
                                    &nbsp;
                                @endif
                                </div>
                            </div>
                        </div>
                        <div id="reply_to_{{ $reply->id }}"></div>

                        @if ($reply->replys)
                        @foreach ($reply->replys as $reply_to)
                        <div class="nested" name="nested-{{ $reply->id }}">
                            <div class="comment" id="c-{{ $reply_to->id }}">

                                <div class="avatar portrait-b">
                                    <div class="avatar-interior">
                                        <a href="javascript: void(0);">
                                            <img width="64" height="64" src="{{ $reply_to->character()->get_url_avatar() }}" alt="{{ $reply_to->character()->name }}" class="wow-class-{{ $reply_to->character()->class }}" />
                                        </a>
                                    </div>
                                </div>

                                <div class="comment-interior">
                                    <div class="character-info user">
                                        <div class="user-name">
                                            <a href="javascript: void(0);" class="context-link wow-class-{{ $reply_to->character()->class }}" rel="np">
                                                {{ $reply_to->character()->name }}
                                            </a>
                                        </div>
                                        <span class="time">
                                            <a href="#c-{{ $reply_to->id }}">{{ $reply_to->created_at->format('d \d\e M \d\e Y H:i') }}</a>
                                        </span>
                                    </div>
                                    <div class="content @if ($reply_to->user->level(3, '>='))wow-class-moderator@endif"><pre>{{ $reply_to->text }}</pre>
                                    </div>

                                    <div class="comment-actions">
                                    @if (Auth::check())
                                        @if (Auth::user()->can('create_blog_reply') && $character)
                                            <a class="reply-link" href="javascript: void(0);" onclick="Blog.reply_add({{ $reply_to->id }}, {{ $reply->id }}, '{{ $reply_to->character()->name }}')">Responder</a>
                                        @endif
                                        @if (Auth::user()->can('delete_blog_reply') || $reply_to->user == Auth::user())
                                            <a href="javascript: void(0);" onclick="Blog.reply_delete({{ $reply_to->id }})">Excluir</a>
                                        @endif
                                    @else
                                        &nbsp;
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="reply_to_{{ $reply_to->id }}"></div>
                        @endforeach
                        @endif

                        @endforeach
                        <span class="clear"><!-- --></span>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

    <div id="right">
        @include('sidebars')
    </div>
    <span class="clear"><!-- --></span>
</div>

@endsection