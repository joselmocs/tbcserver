<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('page_title')</title>

    {{ HTML::style('/css/common.css') }}
    {{ HTML::style('/css/wow.css') }}
    @yield('styles')
</head>

<body class="@yield('body_class')">
<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />

<div id="wrapper">
<div id="header">
    <div class="search-bar">
        <form action="http://us.battle.net/wow/pt/search" method="get" autocomplete="off">
            <div>
                <div class="ui-typeahead-ghost"><input type="text" value="" autocomplete="off" readonly="readonly" class="search-field input input-ghost" /><input type="text" class="search-field input" name="q" id="search-field" maxlength="200" tabindex="40" alt="Buscar personagens, itens, fóruns e mais..." value="Buscar personagens, itens, fóruns e mais..." style="background-color: transparent;" /></div>
                <input type="submit" class="search-button" value="" tabindex="41" />
            </div>
        </form>
    </div>
    <h1 id="logo"><a href="">World of Warcraft®</a></h1>
    <div class="header-plate">
        <ul class="menu" id="menu">
            <li class="menu-home">
                <a href="/" class="menu-active">
                    <span>Início</span>
                </a>
            </li>
            <li class="menu-game">
                <a href="jogo.html">
                    <span>Registrar</span>
                </a>
            </li>
            <li class="menu-community">
                <a href="comunidade.html">
                    <span>Comunidade</span>
                </a>
            </li>
            <li class="menu-media">
                <a href="midia.html">
                    <span>Mídia</span>
                </a>
            </li>
            <li class="menu-forums">
                <a href="forum.html">
                    <span>Fóruns</span>
                </a>
            </li>
            <li class="menu-services">
                <a href="loja.html">
                    <span>Loja</span>
                </a>
            </li>
        </ul>
        <div class="user-plate">
            <div class="card-character plate-offline">
                <div class="status-server">
                    <div><strong>Login Server:</strong> <span id="server-status">Offline</span></div>
                    <div style="display: none;"><strong>Online:</strong> <span id="server-players">0</span> players</div>
                    <div style="display: none;"><strong>Uptime:</strong> <span id="server-uptime">-</span></div>
                </div>
            </a>
        </div>
    </div>
    <div class="new-feature-tip" id="feature-tip" style="display: none">
        <a href="http://us.battle.net/wow/pt/zone/" data-label="World of Warcraft Game Guide" class="clickable">
            <span class="title">Atualizado!</span>
            <strong>Cenários </strong>
            <span class="desc">Confira nosso guia com informações sobre os novos cenários em Mists of Pandaria!</span>
        </a>
    </div>

</div>
<div id="content">
    <div class="content-top">
        <div class="content-bot">
            @yield('conteudo')
        </div>
    </div>
</div>

<div id="footer">
    <div id="copyright">
        <span>©2013 Blizzard Entertainment, Inc. Todos os direitos reservados.</span>
        <a target="_blank" href="" tabindex="100">Termos de Uso</a>
        <a target="_blank" href="" tabindex="100">Legal</a>
        <a target="_blank" href="" tabindex="100">Política de Privacidade</a>
    </div>
</div>

</div>

{{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js') }}
{{ HTML::script('/js/server.js') }}
@yield('scripts')

</body>
</html>