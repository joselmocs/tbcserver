
<div class="summary-stats-profs-bgs">
    <div class="summary-stats" id="summary-stats">
        <div class="summary-stats-advanced">
            <div class="summary-stats-column">
                <h4>{{ Config::get('server.sv.realmd.name') }}</h4>
                <ul>
                    <li class="">
                        <span class="name">XP Rate</span>
                        <span class="value color-q2">{{ Config::get('server.sv.rate.xp') }}</span>
                        <span class="clear"><!-- --></span>
                    </li>
                    <li class="">
                        <span class="name">Drop, Gold Rate</span>
                        <span class="value color-q2">{{ Config::get('server.sv.rate.dropgold') }}</span>
                        <span class="clear"><!-- --></span>
                    </li>
                    <li class="">
                        <span class="name">Patch</span>
                        <span class="value color-q2">{{ Config::get('server.sv.patch') }}</span>
                        <span class="clear"><!-- --></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="summary-stats-end"></div>
    </div>
</div>

<div class="coc-realmlist">
    set realmlist <span>{{ Config::get('server.sv.realmd.url') }}</span>
</div>

<div id="sidebar-marketing" class="sidebar-module">
    <div class="bnet-offer">
        <div class="bnet-offer-bg">
            <a href="/server/how-to-connect" class="bnet-offer-image">
                <img src="/images/como-conectar.jpg" width="300" height="130" alt="" />
            </a>
        </div>
    </div>
</div>

<div class="sidebar" id="sidebar">

    <div class="sidebar-top">
        <div class="sidebar-bot">




            @if (Auth::check())
            <div class="sidebar-module " id="sidebar-under-dev" style="">
                <div class="sidebar-title">

                </div>

                <ul class="sidebar-auth-user">
                    <li>
                        <a href="/dashboard" class="paid-services-minhaconta">
                            <span>Minha Conta</span>
                        </a>
                    </li>
                    <li>
                        <a href="/user/logout" class="paid-services-logout">
                            <span>Sair</span>
                        </a>
                    </li>
                </ul>

                <div class="clear"><!-- --></div>

            </div>
            @endif

            @if (!Auth::check())
            <div class="sidebar-module " id="sidebar-under-dev" style="">
                <div class="sidebar-title">
                    <h3 class="category title-under-dev">Entrar</h3>
                </div>

                @if (count($errors) > 0)
                <div id="errors">
                    <ul>
                        @foreach ($errors->all() as $e)
                        <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="sidebar-content login">
                    <form method="post" action="{{ URL::to('user/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="left">
                            <div>Usuário <br /><input type="text" alt="Nome" name="username" class="" /></div>
                            <div>Senha <br /><input type="password" alt="Nome" name="password" class="" /></div>
                        </div>

                        <div class="right">
                            <a class="ui-button button3 float-right " href="/user/retrieve">
                                <span>
                                    <span>Recuperar Conta</span>
                                </span>
                            </a>
                        </div>

                        <div class="clear"><!-- --></div>

                        <button class="ui-button button1 button1-next" type="submit">
                                <span>
                                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;entrar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </span>
                        </button>

                        <a class="ui-button button1 float-right " href="/user/register">
                            <span>
                                <span>Criar Conta</span>
                            </span>
                        </a>
                    </form>
                </div>
            </div>
            @endif

            <div id="sidebar-marketing" class="sidebar-module">
                <div class="bnet-offer">
                    <div class="bnet-offer-bg">
                        <a href="/server/features" class="bnet-offer-image">
                            <img src="/images/caracteristicas.jpg" width="300" height="130" alt="" />
                        </a>
                    </div>
                </div>
            </div>

            <div class="sidebar-module " id="sidebar-forums" style="">
                <div class="sidebar-title">
                    <h3 class="category title-forums">						<a href="http://us.battle.net/wow/pt/forum/">

                            Discussões Recentes


                        </a>

                    </h3>
                </div>

                <div class="sidebar-content">

                    <ul class="trending-topics">



                        <li>
                            <a href="http://us.battle.net/wow/pt/forum/topic/9245734865" class="topic">
                                [LOVECAST] Recadinhos de Amor!
                            </a>

                            <a href="http://us.battle.net/wow/pt/forum/3772006/" class="forum">
                                Geral
                            </a>

                            -

                            <span class="date">06/06/13 13:51</span>
                        </li>
                        <li>
                            <a href="http://us.battle.net/wow/pt/forum/topic/9245744561" class="topic">
                                Do que mais sente falta quando era newbie no WOW?
                            </a>

                            <a href="http://us.battle.net/wow/pt/forum/3772006/" class="forum">
                                Geral
                            </a>

                            -

                            <span class="date">05/06/13 10:27</span>
                        </li>
                    </ul>

                </div>
            </div></div>
    </div>
</div>
