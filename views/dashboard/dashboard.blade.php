@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection
@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/dashboard.css') }}
@endsection

@section('scripts')
{{ HTML::script('/js/dashboard.js') }}
@endsection

@section('conteudo')

@if ($character)
<div id="profile-wrapper" class="profile-wrapper @if ($character->isHorde()) horde @else alliance @endif">
@endif

<div class="profile-sidebar-anchor">
    <div class="profile-sidebar-outer">
        <div class="profile-sidebar-inner">
            <div class="profile-sidebar-contents">

                <div class="profile-info-anchor profile-guild-info-anchor">
                    <div class="guild-tabard">
                        <div>
                            <div class="left">Status da Conta:</div>
                            <div class="right online"><strong>Ativo</strong></div>
                            <span class="clear"><!-- --></span>
                        </div>
                        <div>
                            <div class="left">Último login:</div>
                            <div class="right">
                                @if (Auth::user()->account->last_ip == "0.0.0.0")
                                -
                                @else
                                {{ date('d/m/Y H:s', strtotime(Auth::user()->account->last_login)) }}
                                @endif
                            </div>
                            <span class="clear"><!-- --></span>
                        </div>
                        <div>
                            <div class="left">Último IP:</div>
                            <div class="right">
                                @if (Auth::user()->account->last_ip == "0.0.0.0")
                                -
                                @else
                                {{ Auth::user()->account->last_ip }}
                                @endif
                            </div>
                            <span class="clear"><!-- --></span>
                        </div>
                        <div>
                            <div class="left">E-mail registrado:</div>
                            <div class="right">{{ Auth::user()->email_mask() }}</div>
                        </div>
                    </div>

                    <div class="profile-info profile-guild-info">
                        <div class="name"><a href="/dashboard">Gerenciador de Conta</a></div>
                        <div class="under-name">
                            <a style="font-weight: bold;">{{ Auth::user()->username }}</a>
                        </div>
                        <div class="div-achv-gc">
                            <div class="achievements"><a>{{ Auth::user()->profile->vote_points }}</a> Votos</div>
                            <div class="achievements"><a style="color: #ffb100;">{{ Auth::user()->profile->game_coins }}</a> Moedas</div>
                        </div>
                    </div>
                </div>


                <ul class="profile-sidebar-menu" id="profile-sidebar-menu">
                    <li class="title-menu">
                        <a href="javascript: void(0)" rel="np">
                            <span class="arrow"><span class="icon">Minha Conta</span></span>
                        </a>
                    </li>

                    <li class="@if ($mode == null) active @endif">
                        <a href="/dashboard" class="" rel="np">
                            <span class="arrow"><span class="icon">Início</span></span>
                        </a>
                    </li>

                    <li class="@if ($mode == 'password') active @endif">
                        <a href="/dashboard/password">
                            <span class="arrow"><span class="icon">Troca de Senha</span></span>
                        </a>
                    </li>

                    <li>
                        <a href="/user/logout" class="back-to" rel="np">
                            <span class="arrow"><span class="icon">Logout</span></span>
                        </a>
                    </li>
                </ul>

                <div class="clear" style="margin-top: 30px;"><!-- --></div>

                @if ($character)
                <ul class="profile-sidebar-menu" id="profile-sidebar-menu">
                    <li class="title-menu">
                        <a href="javascript: void(0)" rel="np">
                            <span class="arrow"><span class="icon">{{ $character->name }}</span></span>
                        </a>
                    </li>

                    <li class="@if ($mode == 'change-name') active @endif">
                        <a href="/dashboard/change-name">
                            <span class="arrow"><span class="icon">Troca de Nome</span></span>
                        </a>
                    </li>

                    <li class="@if ($mode == 'change-race') active @endif">
                        <a href="/dashboard/change-race">
                            <span class="arrow"><span class="icon">Troca de Raça</span></span>
                        </a>
                    </li>

                    <li class="@if ($mode == 'unstuck') active @endif">
                        <a href="/dashboard/unstuck">
                            <span class="arrow"><span class="icon">Unstuck</span></span>
                        </a>
                    </li>
                </ul>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="profile-contents">

<div class="summary">

<div class="profile-section">

<div class="summary-right">


<div class="summary-characterspecific">

    <div class="summary-simple-list">
        @if ($character)
            Perfil Ativo
        @else
            Selecione um personagem abaixo para usá-lo como perfil no site:
        @endif
        <div class="profile-box-simple perfil"></div>
    </div>
    @if ($character)
    <div class="summary-character">
        <a class="avatar" href="" rel="np">
            <img src="{{ $character->get_url_avatar() }}" width="84" height="84" />
        </a>
        <div class="rest">
            <div class="name"><a href="" rel="np">{{ $character->name }}</a></div>
            <div class="reputation">
                <div class="guild-progress guild-replevel-3">
                        <span class="description">{{ $character->get_str_race() }} <strong class="wow-class-{{ $character->class }}">{{ $character->get_str_class() }}</strong> {{ $character->level }}</span>
                </div>
            </div>
        </div>

        <span class="clear"><!-- --></span>
    </div>
    @endif
</div>


<div class="summary-simple-list summary-perks">
    <h3 class="category ">Alterar Perfil</h3>
    <form action="/dashboard/profile/change" method="post" id="change-profile">
        <input type="hidden" name="character" value="" />
        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
    </form>
    <div class="profile-box-simple">
        <ul>
            @foreach(Auth::user()->account->characters as $my_char)
            <li>
                <a href="javascript: void(0);" name="change-profile" profile="{{ $my_char->guid }}">
                    <span class="icon-wrapper">
                        <img src="{{ $my_char->get_url_avatar() }}" width="40" height="40" />
                    </span>
                    <div class="text">
                        <strong>{{ $my_char->name }}</strong>
                        <span class="desc">{{ $my_char->get_str_race() }} <strong class="wow-class-{{ $my_char->class }}">{{ $my_char->get_str_class() }}</strong></span>
                    </div>
                    <span class="type">Nível {{ $my_char->level }}</span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
            @endforeach
        </ul>
        <span class="clear"><!-- --></span>
    </div>
</div>

<div class="summary-weekly-contributors">
    <h3 class="category ">Lista de Amigos</h3>

    <div class="profile-box-simple">


        <div id="roster" class="table amigos">
            <table>
                <thead>
                <tr>
                    <th class="name align-center">
                        <span class="sort-tab">Nome</span>
                    </th>
                    <th class="cls align-center">
                        <span class="sort-tab">Classe</span>
                    </th>
                    <th class="lvl align-center">
                        <span class="sort-tab">Nível</span>
                    </th>
                    <th class="weekly align-center">
                        <span class="sort-tab">Status</span>
                    </th>
                </tr>
                </thead>
                <tbody>

                @if ($character && $character->friends())
                    @foreach($character->friends() as $friend)
                    <tr class="row1" data-level="85">
                        <td class="name">
                            <a href="javascript: void(0);" class="color-c1">{{ $friend->name }}</a>
                        </td>
                        <td class="cls" style="text-align: center;">
                            <span class="icon-frame frame-14 ">
                                <img src="/images/icons/class/class_{{ $friend->class }}.jpg" alt="" width="14" height="14" />
                            </span>
                        </td>
                        <td class="lvl">{{ $friend->level }}</td>
                        @if ($friend->online)
                        <td class="weekly online">Online</td>
                        @else
                        <td class="weekly offline">Offline</td>
                        @endif
                    </tr>
                    @endforeach
                @else
                    <tr class="row1" data-level="85">
                        <td colspan="4">
                            Nenhum amigo encontrado.
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>
        </div>


        <span class="clear"><!-- --></span>


    </div>
</div>

</div>

<div class="summary-left">

@if ($mode == null)
    @include('dashboard/index')
@elseif ($mode == "password")
    @include('dashboard/password')
@elseif ($mode == "change-name")
    @include('dashboard/change-name')
@endif

</div>

<span class="clear"><!-- --></span>
</div>
</div>

</div>

<span class="clear"><!-- --></span>
</div>


@endsection