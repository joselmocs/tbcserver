
<div class="alert info closeable border-4 glow-shadow" style="margin-top: 1px;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong>Importante</strong>
            </p>
            <ul>
                <li>A senha no jogo também será alterada.</li>
                <li>A equipe {{ Config::get('server.site.name') }} nunca irá pedir a sua senha.</li>
                <li>A segurança da conta é de responsabilidade exclusiva do usuário.</li>
                <li>Será enviado um comunicado ao email associado a esta conta.</li>
            </ul>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div class="alert error closeable border-4 glow-shadow" id="erros" style="display: none;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong><a name="form-errors"> </a>Os seguintes erros ocorreram:</strong>
            </p>
            <ul></ul>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div>
    <h3 class="category" id="change-pass-title">Troca de Senha</h3>
</div>

<div class="alert success closeable border-4 glow-shadow" id="password-change-success" style="display: none;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong>Senha realizada com sucesso</strong>
            </p>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div class="sidebar-module profile-box-simple" id="change-pass-content">

    <span class="label-text">
        Senha Atual:
    </span>

    <span class="input-right">
        <span class="input-text input-text-small">
            <input type="password" name="actual-password" id="actual-password" class="small border-5" maxlength="32" placeholder="Insira a senha atual" />
            <span class="inline-message"> </span>
        </span>
        <span class="input-text input-text-small"></span>
    </span>

    <span class="label-text">
        Nova Senha:
    </span>

    <span class="input-right">
        <span class="input-text input-text-small">
            <input type="password" name="new-password" id="new-password" class="small border-5" maxlength="32" placeholder="Insira a nova senha" />
            <span class="inline-message"> </span>
        </span>
        <span class="input-text input-text-small">
            <input type="password" name="new-password_confirm" id="new-password_confirm" class="small border-5" maxlength="32" placeholder="Reinsira a nova senha" />
        </span>
    </span>

    <div id="submit-buttons">
        <button class="ui-button button1 button1-next" id="submit-change-pass" type="button">
            <span>
                <span>Alterar minha senha</span>
            </span>
        </button>

        <a class="ui-button button3 float-right cancel-change-pass" href="/dashboard">
            <span>
                <span>Cancelar</span>
            </span>
        </a>
    </div>

    <div class="reg-loader">
        <div class="img">
            <img src="/images/loaders/canvas-loader.gif" />
        </div>
        <div>
            Por favor aguarde...
        </div>
    </div>

    <div class="clear" style="margin: 20px 0;"><!-- --></div>

</div>