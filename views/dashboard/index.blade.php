
<div class="summary-motd">
    <div class="description">
        <ul>
            <li>Manutenção programada todas as terças feiras às zero horas (meia noite).</li>
        </ul>
    </div>
</div>


<div class="summary-simple-list summary-perks">
    <h3 class="category ">Vote e Ganhe Pontos</h3>

    <div class="profile-box-simple">

        <ul>
            <li>
                <a href="@if ($uservotes['openwow']->can_vote())/vote/openwow@endif" style="height: 56px;">
                    <span class="icon-wrapper">
                        <img src="/images/vote/openwow.jpg" />
                    </span>
                    <span class="type">
                        @if ($uservotes['openwow']->can_vote())
                        vote agora e ganhe 2 pontos
                        @else
                        Aguarde {{ $uservotes['openwow']->str_wait_time() }}
                        @endif
                    </span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
            <li>
                <a href="@if ($uservotes['xtremetop100']->can_vote())/vote/xtremetop100@endif" style="height: 56px;">
                    <span class="icon-wrapper">
                        <img src="/images/vote/xtremetop100.jpg" />
                    </span>
                    <span class="type">
                        @if ($uservotes['xtremetop100']->can_vote())
                        vote agora e ganhe 2 pontos
                        @else
                        Aguarde {{ $uservotes['xtremetop100']->str_wait_time() }}
                        @endif
                    </span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
            <li>
                <a href="@if ($uservotes['gamesites200']->can_vote())/vote/gamesites200@endif" style="height: 56px;">
                    <span class="icon-wrapper">
                        <img src="/images/vote/gamesites200.gif" />
                    </span>
                    <span class="type">
                        @if ($uservotes['gamesites200']->can_vote())
                        vote agora e ganhe 2 pontos
                        @else
                        Aguarde {{ $uservotes['gamesites200']->str_wait_time() }}
                        @endif
                    </span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
            <li>
                <a href="@if ($uservotes['top100arena']->can_vote())/vote/top100arena@endif" style="height: 56px;">
                    <span class="icon-wrapper">
                        <img src="/images/vote/top100arena.png" />
                    </span>
                    <span class="type">
                        @if ($uservotes['top100arena']->can_vote())
                        vote agora e ganhe 2 pontos
                        @else
                        aguarde {{ $uservotes['top100arena']->str_wait_time() }}
                        @endif
                    </span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
        </ul>

        <div id="vote">
            <p>Com os pontos você pode trocar por itens, trocar de raça e mudar seu nome.</p>
            <ul>
                <li>Você pode votar uma vez a cada 12 horas em cada um dos sites acima.</li>
                <li>Você ganhará 2 pontos para cada voto que fizer nos sites acima.</li>
            </ul>
            <p><b>Importante:</b> Para que o voto seja computado é necessário clicar em um site acima, preencher o captcha e clicar no banner {{ Config::get('server.sv.realmd.name') }} para retornar.</p>
        </div>


    </div>
</div>