
<input type="hidden" name="character_guid" value="{{ $character->guid }}" />

<div class="alert info closeable border-4 glow-shadow" style="margin-top: 1px;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong>Importante</strong>
            </p>
            <ul>
                <li>Esta operação custa <b>{{ Config::get('server.cost.change.name') }}</b> ponto(s) de <b>VOTO</b>.</b></li>
                <li>Uma vez concluída, a mudança de nome não pode ser revertida, exceto por uma segunda operação de mudança de nome (sujeito aos mesmos custos e limitações).</li>
            </ul>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div class="alert error closeable border-4 glow-shadow" id="erros" style="display: none;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong><a name="form-errors"> </a>Os seguintes erros ocorreram:</strong>
            </p>
            <ul></ul>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div class="summary-simple-list summary-perks" style="height: 140px;">
    <h3 class="category ">Troca de Nome</h3>

    <div class="profile-box-simple" style="height: 70px;">
        <ul>
            <li>
                <a style="height: 70px;">
                    <span class="icon-wrapper">
                        <img src="{{ $character->get_url_avatar() }}" width="64" height="64" />
                    </span>
                    <div class="text" style="margin-left: 20px;">
                        <strong style="font-size: 18px;">{{ $character->name }}</strong>
                        <span class="desc">{{ $character->get_str_race() }} <strong class="wow-class-5">{{ $character->get_str_class() }}</strong></span>
                    </div>
                    <span class="type">Nível {{ $character->level }}</span>
                    <span class="clear"><!-- --></span>
                </a>
            </li>
        </ul>

        <span class="clear"><!-- --></span>
    </div>
</div>

<div class="alert success closeable border-4 glow-shadow" id="name-change-success" style="display: none;">
    <div class="alert-inner">
        <div class="alert-message">
            <p class="title">
                <strong>Operação realizada com sucesso</strong>
            </p>
            <ul>
                <li>Um novo nome será requisitado dentro do jogo ao efetuar o próximo login.</li>
            </ul>
        </div>
    </div>
    <span class="clear"><!-- --></span>
</div>

<div id="submit-buttons">
    <button class="ui-button button1 button1-next" id="submit-change-name" type="button">
        <span>
            <span style="width: 290px;">Aceito e quero continuar</span>
        </span>
    </button>
    <a class="ui-button button3 float-right cancel-change-pass" href="/dashboard">
        <span>
            <span>Cancelar</span>
        </span>
    </a>
</div>

<div class="reg-loader">
    <div class="img">
        <img src="/images/loaders/canvas-loader.gif" />
    </div>
    <div>
        Por favor aguarde...
    </div>
</div>
