@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/cadastro.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        Ativação de Conta
                    </h3>
                    <div class="byline">
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Ativação de Conta" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <span class="input-right obrigado">
                        <div class="title">
                            {{ $response }}
                        </div>
                    </span>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span id="comments"></span>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection