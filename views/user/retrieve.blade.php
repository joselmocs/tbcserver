@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/cadastro.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('scripts')
{{ HTML::script('/js/retrieve.js') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        Recuperação de Conta
                    </h3>
                    <div class="byline">
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Recuperação de Senha" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <span class="input-right obrigado" style="display: none;">
                        <div class="title">
                            E-mail enviado!
                        </div>
                        <div class="clear"><!-- --></div>
                        <label for="agreedToToU" class="termos">
                            <span class="label-text">
                                As informações para a recuperação/ativação da sua conta foi enviado para seu endereço de e-mail.<br />
                                Por favor, verifique a Caixa de Entrada e o Spam do seu email.
                            </span>
                        </label>
                    </span>

                    <form method="post" id="user-retrieve" onsubmit="javascript: return false;">

                    <div class="alert error closeable border-4 glow-shadow" id="erros" style="display: none;">
                        <div class="alert-inner">
                            <div class="alert-message">
                                <p class="title">
                                    <strong><a name="form-errors"> </a>Os seguintes erros ocorreram:</strong>
                                </p>
                                <ul></ul>
                            </div>
                        </div>
                    </div>

                    <div class="detail form-cadastro">

                        <span class="label-text" id="help_inf" style="margin-bottom: 20px;">
                            Informe seu nome de usuário para recuperar a senha
                        </span>

                        <span class="input-right">
                            <span class="input-select input-select-small">
                                <select name="tipo_recup" id="tipo_recup" class="small border-5">
                                    <option value="password" selected="selected">Senha</option>
                                    <option value="username">Nome de Usuário</option>
                                    <option value="activation">E-mail de Ativação</option>
                                </select>
                            </span>
                            <span class="input-text input-text-small">
                                <input type="text" name="informado" id="informado" class="small border-5" maxlength="32" placeholder="Nome de Usuário" />
                                <span class="inline-message " id="informado-message"> </span>
                            </span>
                        </span>

                        <button class="ui-button button1 button1-next" id="submit-ret" type="button">
                            <span>
                                <span>Recuperar</span>
                            </span>
                        </button>

                        <div class="reg-loader">
                            <div class="img">
                                <img src="/images/loaders/canvas-loader.gif" />
                            </div>
                            <div>
                                Por favor aguarde...
                            </div>
                        </div>
                        <div class="clear"><!-- --></div>

                    </div>
                    </form>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span id="comments"></span>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection