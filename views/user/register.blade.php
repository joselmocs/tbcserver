@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/cadastro.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('scripts')
{{ HTML::script('/js/register.js') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        Criação de Conta
                    </h3>
                    <div class="byline">
                        <div class="blog-info">
                            Nós valorizamos e respeitamos sua privacidade. Seu nome ou email não será exibido para outros usuários.
                        </div>
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Criação de Conta" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <span class="input-right obrigado" style="display: none;">
                        <div class="title">
                            Obrigado por se cadastrar no {{ Config::get('server.site.name') }}!
                        </div>
                        <div class="clear"><!-- --></div>
                        <label for="agreedToToU" class="termos">
                            <span class="label-text">
                                Um email de confirmação foi enviado para seu endereço de e-mail. <br />
                                Por favor, verifique seu e-mail para clique no link de ativação.
                            </span>
                        </label>
                        </span>

                    <form method="post" id="user-register" onsubmit="javascript: return false;">
                    <div class="alert error closeable border-4 glow-shadow" id="erros" style="display: none;">
                        <div class="alert-inner">
                            <div class="alert-message">
                                <p class="title">
                                    <strong><a name="form-errors"> </a>Os seguintes erros ocorreram:</strong>
                                </p>
                                <ul></ul>
                            </div>
                        </div>
                    </div>

                    <div class="detail form-cadastro">

                        <span class="label-text">
                            Nome de usuário:
                        </span>

                        <span class="input-right">
                            <span class="input-text input-text-small">
                                <input type="text" name="username" id="username" class="small border-5" maxlength="16" placeholder="Nome de usuário" help="Este será o nome utilizado para logar no site e no jogo." />
                                <span class="inline-message " id="username-message"> </span>
                            </span>
                            <span class="input-text input-text-small"></span>
                        </span>

                        <span class="label-text">
                            Nome:
                        </span>

                        <span class="input-right">
                            <span class="input-text input-text-small">
                                <input type="text" name="firstname" id="firstname" class="small border-5" maxlength="32" placeholder="Primeiro nome" />
                                <span class="inline-message " id="firstname-message"> </span>
                            </span>
                            <span class="input-text input-text-small">
                                <input type="text" name="lastname" id="lastname" class="small border-5" maxlength="32" placeholder="Sobrenome" />
                                <span class="inline-message " id="lastname-message"> </span>
                            </span>
                        </span>

                        <span class="label-text">
                            Endereço de e-mail:
                        </span>

                        <span class="input-right">
                            <span class="input-text input-text-small">
                                <input type="text" name="email" id="email" class="small border-5" maxlength="32" placeholder="Insira o endereço de e-mail" help="Este email será usado para ativar e recuperar sua conta caso necessite." />
                                <span class="inline-message " id="email-message"> </span>
                            </span>
                            <span class="input-text input-text-small">
                                <input type="text" name="email_confirm" id="email_confirm" class="small border-5" maxlength="32" placeholder="Reinsira o endereço de e-mail" />
                                <span class="inline-message " id="email_confirm-message"> </span>
                            </span>
                        </span>

                        <span class="label-text">
                            Senha:
                        </span>

                        <span class="input-right">
                            <span class="input-text input-text-small">
                                <input type="password" name="password" id="password" class="small border-5" maxlength="32" placeholder="Insira a senha" />
                                <span class="inline-message " id="password-message"> </span>
                            </span>
                            <span class="input-text input-text-small">
                                <input type="password" name="password_confirm" id="password_confirm" class="small border-5" maxlength="32" placeholder="Reinsira a senha" />
                                <span class="inline-message " id="password_confirm-message"> </span>
                            </span>
                        </span>

                        <button class="ui-button button1 button1-next" id="submit-reg" type="button">
                            <span>
                                <span>Crie uma Conta Grátis</span>
                            </span>
                        </button>

                        <div class="reg-loader">
                            <div class="img">
                                <img src="/images/loaders/canvas-loader.gif" />
                            </div>
                            <div>
                                Por favor aguarde...
                            </div>
                        </div>
                        <div class="clear"><!-- --></div>

                        <span class="input-right">
                            <label for="agreedToToU" class="termos">
                                <span class="label-text">
                                    Ao registrar uma conta, você concorda com nossos <a href="" onclick="">Termos de Uso</a>.<span class="input-required">*</span>
                                </span>
                            </label>
                        </span>

                    </div>
                    </form>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span id="comments"></span>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection