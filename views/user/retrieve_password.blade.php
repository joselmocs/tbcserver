@extends('template')

@section('page_title')
{{ Config::get('server.site.title') }}
@endsection

@section('body_class')homepage @endsection

@section('styles')
{{ HTML::style('/css/cadastro.css') }}
{{ HTML::style('/css/blog.css') }}
{{ HTML::style('/css/cms.css') }}
@endsection

@section('scripts')
{{ HTML::script('/js/retrieve.pass.js') }}
@endsection

@section('conteudo')

<div id="blog-wrapper">
    <div id="left">
        <div id="blog-container">
            <div id="blog">
                <div class="blog-inner">
                    <h3 class="blog-title">
                        Alteração de Senha
                    </h3>
                    <div class="byline">
                        <span class="clear"><!-- --></span>
                    </div>
                    <div class="header-image"><img alt="Recuperação de Senha" src="http://bnetcmsus-a.akamaihd.net/cms/blog_header/GFTVZT71U10T1318640466014.jpg" /></div>

                    <span class="input-right obrigado" style="display: none;">
                        <div class="title">
                            Senha alterada com sucesso!
                        </div>
                        <div class="clear"><!-- --></div>
                        <label for="agreedToToU" class="termos">
                            <span class="label-text">
                                Você já pode autenticar no site usando sua nova senha.
                            </span>
                        </label>
                    </span>

                    <form method="post" id="pass-retrieve" onsubmit="javascript: return false;">
                        <input type="hidden" name="_username" value="{{ $username }}" />
                        <input type="hidden" name="_salt" value="{{ $salt }}" />

                    <div class="alert error closeable border-4 glow-shadow" id="erros" style="display: none;">
                        <div class="alert-inner">
                            <div class="alert-message">
                                <p class="title">
                                    <strong><a name="form-errors"> </a>Os seguintes erros ocorreram:</strong>
                                </p>
                                <ul></ul>
                            </div>
                        </div>
                    </div>

                    <div class="detail form-cadastro">

                        <span class="label-text">
                            Insira sua nova Senha:
                        </span>

                        <span class="input-right">
                            <span class="input-text input-text-small">
                                <input type="password" name="password" id="password" class="small border-5" maxlength="32" placeholder="Insira a senha" />
                                <span class="inline-message " id="password-message"> </span>
                            </span>
                            <span class="input-text input-text-small">
                                <input type="password" name="password_confirm" id="password_confirm" class="small border-5" maxlength="32" placeholder="Reinsira a senha" />
                                <span class="inline-message " id="password_confirm-message"> </span>
                            </span>
                        </span>

                        <button class="ui-button button1 button1-next" id="submit-ret-pass" type="button">
                            <span>
                                <span>Alterar</span>
                            </span>
                        </button>

                        <div class="reg-loader">
                            <div class="img">
                                <img src="/images/loaders/canvas-loader.gif" />
                            </div>
                            <div>
                                Por favor aguarde...
                            </div>
                        </div>
                        <div class="clear"><!-- --></div>

                    </div>
                    </form>

                    <div class="keyword-list"></div>
                </div>
            </div>

            <span id="comments"></span>

            <span class="clear"><!-- --></span>
        </div>
    </div>
</div>

<div id="right">
    @include('sidebars')
</div>

<span class="clear"><!-- --></span>

@endsection