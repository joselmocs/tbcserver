<p>Olá {{ $user->profile->firstname }},</p>

<p>Obrigado por se registar em {{ $site_name }}. Sua conta foi criada e deve ser ativada antes que você possa usá-lo.</p>

<p>Para ativar a conta clique no link a abaixo ou copie e cole no seu navegador:<br />
{{ $url }}/user/activate/{{ strtolower($user->username) }}/{{ $user->salt }}</p>

<p>Após a ativação, você pode fazer o login em {{ $url }} usando as seguintes credenciais:</p>

<p>Usuário: {{ strtolower($user->username) }}<br />
@if($password)Senha: {{ $password }} @endif</p>

<p>Cordialmente,<br />
Equipe {{ $site_name }}</p>