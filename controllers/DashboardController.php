<?php

class DashboardController extends BaseController {

    /**
     * Painel de controle do usuário
     *
     * @param null|string $mode
     * @return View
     */
    public function get_dashboard($mode = null)
    {
        $uservotes = array(
            'openwow' => UserVote::from_site('openwow'),
            'xtremetop100' => UserVote::from_site('xtremetop100'),
            'top100arena' => UserVote::from_site('top100arena'),
            'gamesites200' => UserVote::from_site('gamesites200')
        );

        return View::make('dashboard/dashboard')
            ->with('mode', $mode)
            ->with('character', Auth::user()->profile->character())
            ->with('uservotes', $uservotes);
    }

    /**
     * Altera o personagem do perfil do usuário
     *
     * @return Redirect
     */
    public function post_profile_change()
    {
        $guid = Input::get('character', 0);

        // Verificamos se o personagem informado pertence à conta do usuário logado
        if (Auth::user()->account->check_character((int) $guid)) {
            // Atualizamos o perfil do usuário
            Auth::user()->profile->character_id = (int) $guid;
            Auth::user()->profile->save();
        }

        return Redirect::to(URL::previous());
    }

    /**
     * Envia uma requisição de troca de nome do personagem
     *
     * @return Redirect
     */
    public function post_change_name()
    {
        $guid = Input::get('character', 0);

        $response = array(
            'success' => false,
            'errors' => array("Falha ao tentar mudar o nome do personagem.")
        );

        if (!Auth::user()->profile->has_vote_points('change.name')) {
            array_push($response['errors'], "Você não possui pontos de VOTO suficientes.");
        }
        else {
            // Verificamos se o personagem informado pertence à conta do usuário logado
            if (Auth::user()->account->check_character((int) $guid)) {
                $char = WowCharacter::where('guid', '=', (int) $guid)->first();
                $response = $char->change_name();

                Auth::user()->profile->decrement('vote_points', Config::get('server.cost.change.name'));
            }
        }

        return Response::json($response);
    }

    /**
     * Altera a senha do usuário
     *
     */
    public function post_password_change()
    {
        $input = Input::all();
        $user = Auth::user();

        $validator = Validator::make($input, array(
            'actual_password' => 'required|min:4|max:16',
            'password' => 'required|min:4|max:16',
        ));

        if ($validator->fails()) {
            return Response::json($validator->errors());
        }

        try {
            Auth::validate(array(
                    'username' => $user->username,
                    'password' => Input::get('actual_password'))
            );
        }
        catch (Toddish\Verify\UserPasswordIncorrectException $e) {
            return Response::json(array("Senha informada não confere com a senha atual."));
        }

        // Altera a senha do usuário no site e no servidor
        $user->change_password($input['password']);

        // Envia um email para o usuário comunicando a troca de senha recente em sua conta
        $mail_data = array(
            'user' => $user,
            'url' => Config::get('app.url'),
            'site_name' => Config::get('server.site.name'),
            'tipo_email' => 'password_changed'
        );

        Mail::send('emails.auth.retrieve', $mail_data, function($m) use ($user, $mail_data) {
            $m->to($user->email, $user->profile->firstname)->subject($mail_data['site_name'] .' - Comunicado de troca de senha');
        });

        return Response::json(array(
            'success' => true
        ));
    }

}