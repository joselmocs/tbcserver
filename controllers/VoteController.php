<?php

class VoteController extends BaseController {

    /**
     * Controla a visita vindo do site de voto, incrementando ou não os votos do usuário
     *
     * @param $votesite
     * @return Redirect
     */
    public function get_index($votesite)
    {
        if (Auth::check()) {
            try {
                $user_vote = UserVote::where('user_id', '=', Auth::user()->id)->where('votesite', '=', $votesite);
                if ($user_vote->firstOrFail()->can_vote()) {
                    Auth::user()->profile->increment('vote_points', Config::get('server.vote.pointsgiven'));
                    $user_vote->update(array(
                        'updated_at' => date('Y-m-d H:i:s')
                    ));
                }
                return Redirect::to('dashboard');
            }
            catch (Exception $e) {
                //
            }
        }

        return Redirect::to('/');
    }

}
