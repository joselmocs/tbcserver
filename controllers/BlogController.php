<?php

class BlogController extends BaseController {

    /**
     * Exibe a página principal do site
     *
     * @return View
     */
    public function any_index()
    {
        return View::make('blog/index')
            ->with('blogs', Blog::all());
    }

    /**
     * Exibe a pagina do blog com os comentarios do mesmo
     *
     * @param blog
     * @param slug
     * @return View
     */
    public function get_blog($blog, $slug)
    {
        try {
            $blog = Blog::findOrFail($blog);
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        $replys = BlogReply::where('blog_id', $blog->id)
            ->where('reply_to_id', null)
            ->orderBy('created_at', 'desc')->get();

        $character = null;
        if (Auth::check()) {
            $character = Auth::user()->profile->character();
        }

        return View::make('blog/blog')
            ->with('character', $character)
            ->with('blog', $blog)
            ->with('replys', $replys);
    }

    /**
     * Posta um comentário no blog
     *
     * @return Redirect
     */
    public function post_reply_add()
    {
        if (!Auth::user()->can('create_blog_reply')) {
            App::abort(404);
        }

        $validate = array(
            "blog_id" => "required|integer",
            "text" => "required"
        );

        $validated = Validator::make(Input::all(), $validate);

        if ($validated->fails()) {
            return Redirect::to(Input::get('actual_page'))->withErrors($validated);
        }

        $reply_to = null;
        try {
            Blog::findOrFail(Input::get('blog_id'));

            if (Input::get('reply_to', null)) {
                $reply_to = BlogReply::findOrFail(Input::get('reply_to'));
            }
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            App::abort(404);
        }

        $reply = new BlogReply;
        $reply->user_id = Auth::user()->id;
        $reply->blog_id = Input::get('blog_id');
        $reply->character_id = Auth::user()->profile->character()->guid;
        if ($reply_to) {
            $reply->reply_to_id = $reply_to->id;
        }
        $reply->text = trim(htmlspecialchars(Input::get('text')));
        $reply->save();

        return Redirect::to(Input::get('actual_page') . "#c-". $reply->id);
    }

    /**
     * Deleta um comentário do blog
     *
     * @return Response
     */
    public function post_reply_delete()
    {
        $return = array(
            'success' => false,
            'error' => null
        );

        try {
            $reply = BlogReply::findOrFail(Input::get('reply_id'));
            if (Auth::user()->can('delete_blog_reply') || $reply->user->id == Auth::user()->id) {
                $reply->delete();
                $return['success'] = true;
            }
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $return['error'] = $e;
        }

        return Response::json($return);
    }

}