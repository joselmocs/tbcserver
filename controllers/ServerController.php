<?php

class ServerController extends BaseController {

    /**
     * Retorna uma conexão SOAP feita com o servidor
     *
     * @return View
     */
    public static function SoapClient()
    {
        return new SoapClient(NULL, array(
            'location' => "http://". Config::get('server.soap.url') .":". Config::get('server.soap.port') ."/",
            'uri'      => 'urn:MaNGOS',
            'style'    => SOAP_RPC,
            'login'    => Config::get('server.soap.username'),
            'password' => Config::get('server.soap.password'),
        ));
    }

    /**
     * Exibe a página com as características do servidor
     *
     * @return View
     */
    public function get_features()
    {
        return View::make('server/features');
    }

    /**
     * Exibe a página com as informações para se conectar ao servidor
     *
     * @return View
     */
    public function get_how_to_connect()
    {
        return View::make('server/how-to-connect');
    }

    /**
     * Verifica o estado do servidor, quantidade de players online e uptime
     *
     * @return array
     */
    public function post_status()
    {
        $status = array(
            'status' => false,
            'online' => 0,
            'uptime' => null
        );

        $fsopen = @fsockopen(Config::get('server.sv.realmd.url'), Config::get('server.sv.realmd.port'), $n, $s, 3);
        if ($fsopen) {
            $status['online'] = WowCharacter::total_online();
            $status['status'] = true;

            $starttime = DB::connection('realmd')
                ->table('uptime')
                ->where('realmid', '=', Config::get('server.sv.realmd.id'))
                ->orderBy('starttime', 'desc')
                ->first()->starttime;

            $status['uptime'] = $this->convert_uptime(time() - $starttime);
        }

        return $status;
    }

    /**
     * Converte o tempo em segundos em string legível
     *
     * @param $time
     * @return string
     */
    private function convert_uptime($time)
    {
        if ($time < 60) {
            return $time .' segundos';
        }

        $minutos = $time / 60;
        if ($minutos < 60) {
            return ceil($minutos) .' minutos';
        }

        $horas = floor($minutos / 60);
        if ($horas < 24) {
            $minutos_rest = round($minutos - 60 * $horas);
            $h_plural = ($horas > 1) ? 's' : '';
            $m_plural = ($minutos_rest > 1) ? 's' : '';
            return $horas .' hora'. $h_plural .' e '. ceil($minutos_rest) .' minuto'. $m_plural;
        }

        $dias = floor($minutos / 60 / 24);
        $horas = round(($minutos / 60) - 24 * $dias);

        $d_plural = ($dias > 1) ? 's' : '';
        $h_plural = ($horas > 1) ? 's' : '';

        return $dias .' dia'. $d_plural .' e '. $horas .' hora'. $h_plural;
    }

}
