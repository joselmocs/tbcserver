<?php

return array(
    // Configurações do Site
    'site.name' => "CalangosWoW",
    'site.title' => "CalangosWoW TBC Server",

    // Configuração dos bandos de dados
    'db.realmd' => "realmd",
    'db.characters' => "characters",

    // COnfiguração SOAP
    'soap.url' => "127.0.0.1",
    'soap.port' => "7878",
    'soap.username' => "administrator",
    'soap.password' => "administrator",

    // Configurações do servidor
    'sv.realmd.name' => "CalangosWoW",
    'sv.realmd.id' => 1,
    'sv.realmd.url' => "joselmocs.no-ip.biz",
    'sv.realmd.port' => 3724,
    'sv.rate.xp' => "Blizzlike",
    'sv.rate.dropgold' => "Blizzlike",
    'sv.patch' => "2.4.3",

    // Tempo de espera entre um voto e outro no mesmo site
    'vote.wait_time' => 0.01,

    /// Pontos que o usuário recebe ao votar
    'vote.pointsgiven' => 2,

    'vote.openwow.id' => 1,
    'vote.xtremetop100.id' => 1,
    'vote.top100arena.id' => 1,
    'vote.gamesites200.id' => 1,

    // Preço do serviço de troca de nome
    'cost.change.name' => 5

);
