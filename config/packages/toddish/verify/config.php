<?php
return array(

    'identified_by' => array('username', 'email'),

    // The Super Admin role
    // (returns true for all permissions)
    'super_admin' => 'Super Admin',

    'admin' => 'Admin',
    'moderator' => 'Moderador',
    'user' => 'Usuario',

    // DB prefix for tables
    'prefix' => ''

);
