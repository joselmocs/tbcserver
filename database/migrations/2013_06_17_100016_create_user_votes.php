<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserVotes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users_votes', function($table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('votesite', 32);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users_votes');
	}

}