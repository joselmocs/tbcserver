<?php

use Illuminate\Database\Migrations\Migration;

class CreateBlog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the blogs table
        Schema::create('blogs', function($table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('thumbnail', 256);
            $table->string('image', 256);
            $table->string('title', 128);
            $table->text('text');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        // Create the blogs_reply table
        Schema::create('blogs_reply', function($table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('character_id')->unsigned()->nullable();
            $table->integer('blog_id')->unsigned()->nullable();
            $table->integer('reply_to_id')->unsigned()->nullable();
            $table->text('text');
            $table->timestamps();

            $table->foreign('reply_to_id')->references('id')->on('blogs_reply')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade')->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('blogs_reply');
        Schema::dropIfExists('blogs');
	}

}