<?php

use Illuminate\Database\Migrations\Migration;

class CreateSwearList extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        // Create the users table
        Schema::create('swear_list', function($table)
        {
            $table->increments('id');
            $table->string('name', 16)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swear_list');
    }

}