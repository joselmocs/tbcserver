<?php

use Illuminate\Database\Migrations\Migration;

class ChangeRealmdCharacters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        try {
            // Adicionamos timestamp à tabela account no banco realmd
            Schema::connection(Config::get('server.db.realmd'))->table('account', function($table) {
                $table->integer('user_id')->unsigned()->nullable();
                $table->timestamps();
            });

            // Adicionamos timestamp à tabela characters no banco characters
            Schema::connection(Config::get('server.db.characters'))->table('characters', function($table) {
                $table->timestamps();
            });
        }
        catch (Exception $e) {
            // É lançada uma exeção caso estes campos já existam, neste caso não faremos nada.
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::connection(Config::get('server.db.realmd'))->table('account')
            ->update(array('user_id' => null));
	}

}