<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserProfile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Create the users table
        Schema::create('users_profile', function($table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('character_id')->unsigned()->nullable();
            $table->string('nickname', 32)->default('');
            $table->string('firstname', 32)->default('');
            $table->string('lastname', 32)->default('');
            $table->string('avatar', 256)->default('');
            $table->integer('vote_points')->default(0);
            $table->integer('game_coins')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users_profile');
	}

}