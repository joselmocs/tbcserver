<?php

use Illuminate\Database\Migrations\Migration;
use Toddish\Verify\Models\Permission;
use Toddish\Verify\Models\Role;

class AddBlogPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $blog_c = new Permission;
        $blog_c->name = 'create_blog';
        $blog_c->save();

        $blog_e = new Permission;
        $blog_e->name = 'edit_blog';
        $blog_e->save();

        $blog_d = new Permission;
        $blog_d->name = 'delete_blog';
        $blog_d->save();

        $blog_reply_c = new Permission;
        $blog_reply_c->name = 'create_blog_reply';
        $blog_reply_c->save();

        $blog_reply_e = new Permission;
        $blog_reply_e->name = 'edit_blog_reply';
        $blog_reply_e->save();

        $blog_reply_d = new Permission;
        $blog_reply_d->name = 'delete_blog_reply';
        $blog_reply_d->save();

        // Adiciona as permissoes para usuario
        Role::where('name', '=', Config::get('verify::user'))->first()
            ->permissions()
            ->sync(array($blog_reply_c->id));

        // Adiciona as permissoes para moderador
        Role::where('name', '=', Config::get('verify::moderator'))->first()
            ->permissions()
            ->sync(array($blog_c->id, $blog_e->id, $blog_reply_c->id, $blog_reply_e->id, $blog_reply_d->id));

        // Adiciona as permissoes para administrador
        Role::where('name', '=', Config::get('verify::admin'))->first()
            ->permissions()
            ->sync(array($blog_c->id, $blog_e->id, $blog_d->id, $blog_reply_c->id, $blog_reply_e->id,
                         $blog_reply_d->id));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //
	}

}