<?php

use Illuminate\Database\Migrations\Migration;
use Toddish\Verify\Models\Role;

class AddRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        // Usuário
        $role = new Role;
        $role->name = Config::get('verify::user');
        $role->level = 1;
        $role->save();

        // Moderador
        $role = new Role;
        $role->name = Config::get('verify::moderator');
        $role->level = 3;
        $role->save();

        // Administrador
        $role = new Role;
        $role->name = Config::get('verify::admin');
        $role->level = 5;
        $role->save();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //
	}

}