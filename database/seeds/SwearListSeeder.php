<?php

class SwearListSeeder extends Seeder {

    public function run()
    {
        DB::table('swear_list')->delete();

        $arquivo = __DIR__ ."\\files\\swear_list.txt";

        $f = fopen($arquivo, 'r');
        $conteudo = fread($f, filesize($arquivo));
        fclose($f);

        $nomes = explode("\n", $conteudo);

        foreach ($nomes as $nome) {
            if (trim($nome)) {
                $new = new SwearList;
                $new->name = strtoupper(trim($nome));
                $new->save();
            }
        }
    }
}