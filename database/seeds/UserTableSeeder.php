<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        $user = User::find(1);
        $user->username = "JOSELMO";
        $user->password = "admin";
        $user->email = strtoupper("joselmo@outlook.com");
        $user->account_id = 5;
        $user->save();

        $user->profile->avatar = "/images/avatar/blank.png";
        $user->profile->character_id = 5;
        $user->profile->nickname = ".dot";
        $user->profile->firstname = "Joselmo";
        $user->profile->lastname = "Cardozo";
        $user->profile->vote_points = 100;
        $user->profile->game_coins = 200;
        $user->profile->save();

        DB::connection(Config::get('server.db.realmd'))
            ->table('account')
            ->where('id', '=', 5)
            ->update(array('user_id' => 1));

        $user = User::find(2);
        $user->username = "ALLYSSON";
        $user->password = "admin";
        $user->email = strtoupper("allysson1000@hotmail.com");
        $user->account_id = 6;
        $user->verified = 1;
        $user->save();

        $user->profile->avatar = "/images/avatar/blank.png";
        $user->profile->nickname = "Fromhell";
        $user->profile->firstname = "Allysson";
        $user->profile->lastname = "Assis";
        $user->profile->vote_points = 100;
        $user->profile->game_coins = 200;
        $user->profile->save();

        DB::connection(Config::get('server.db.realmd'))
            ->table('account')
            ->where('id', '=', 6)
            ->update(array('user_id' => 2));
    }
}