<?php

class BlogSeeder extends Seeder {

    public function run()
    {
        DB::table('blogs_reply')->delete();
        DB::table('blogs')->delete();

        Blog::create(array(
            'user_id' => 1,
            'title' => 'Notas do Patch 5.3 - Conflito Iminente',
            'text' => 'As notas do Patch 5.3, Conflito Iminente, estão disponíveis!',
            'thumbnail' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_thumbnail/GSL83UEMRX9E1320442643311.jpg',
            'image' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_header/TAN8CU94KV8P1290560945223.jpg'
        ));

        Blog::create(array(
            'user_id' => 1,
            'title' => 'Um Raide Para Todas as Ocasiões: Prévia de Raide Flexível',
            'text' => 'No World of Warcraft, o raide tem uma longa história de não apenas desafiar os jogadores,' .
            'mas de mudar e evoluir com a passagem dos anos e das expansões. Como grande parte do ' .
            'ciclo de desenvolvimento, chega uma hora em que é necessário ver que novas possibilidades' .
            'podem ser adicionadas ao campo de jogo para melhorar a experiência de ainda mais jogadores.',
            'thumbnail' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_thumbnail/W6KRGRAZCS2S1369356250913.jpg',
            'image' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_header/ENP739POZI2S1369356228164.jpg'
        ));

        Blog::create(array(
            'user_id' => 1,
            'title' => 'Blizzard Insider Nr. 48 — Entrevista com Tom Chilton sobre o Patch 5.3: Conflito Iminente',
            'text' => 'No Patch 5.3: Conflito Iminente, os jogadores enfrentam o reinado agressivo de Garrosh ' .
            'Grito Infernal, outrora orgulhoso Chefe Guerreiro da Horda. Seus crimes são muitos e seus' .
            ' aliados rareiam, mas ele ainda domina Orgrimmar, a capital da Horda, e retém completo ' .
            'domínio de um vasto exército de leais soldados de elite Kor\'kron, sustentados por linhas' .
            ' de abastecimento reforçadas espalhadas pelos Sertões.',
            'thumbnail' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_thumbnail/F7NXGFOD384U1369173094661.jpg',
            'image' => 'http://bnetcmsus-a.akamaihd.net/cms/blog_header/PAYNS8JIPJV71369173217598.jpg'
        ));
    }
}