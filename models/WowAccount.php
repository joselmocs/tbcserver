<?php

class WowAccount extends Eloquent {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'realmd';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'username', 'sha_pass_hash', 'gmlevel', 'sessionkey', 'v', 's', 'email',
        'joindate', 'last_ip', 'locked', 'last_login', 'expansion', 'user_id');

    /**
     * Retorna o usuario que postou a resposta.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna o usuario que postou a resposta.
     *
     * @return User
     */
    public function characters()
    {
        return $this->hasMany('WowCharacter', 'account');
    }

    /**
     * Cria uma conta do usuário
     *
     * @param $user
     * @param $password
     * @return WowAccount
     */
    public static function create_account($user, $password)
    {
        $account_id = WowAccount::insertGetId(array(
            'username' => $user->username,
            'sha_pass_hash' => sha1(strtoupper($user->username. ":" .stripslashes($password))),
            'gmlevel' => 0,
            'email' => $user->email,
            'joindate' => $user->created_at,
            'locked' => 1,
            'expansion' => 1,
            'user_id' => $user->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ));

        $user->account_id = $account_id;
        $user->save();

        return $account_id;
    }

    /**
     * Troca a senha do usuário no servidor
     *
     */
    public function change_password($password)
    {
        $this->sessionkey = "";
        $this->v = 0;
        $this->s = 0;
        $this->sha_pass_hash = sha1(strtoupper($this->user->username. ":" .stripslashes($password)));
        $this->save();
    }

    /**
     * Ativa a conta do usuário no servidor
     *
     */
    public function activate()
    {
        $this->locked = 0;
        $this->save();
    }

    /**
     * Verifica se o personagem informado pertence a esta conta
     *
     * @param $guid
     * @return boolean
     */
    public function check_character($guid)
    {
        try {
            WowCharacter::where('guid', '=', (int) $guid)
                ->where('account', '=', $this->id)
                ->firstOrFail();
            return true;
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return false;
        }
    }

}