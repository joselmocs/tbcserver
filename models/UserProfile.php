<?php

class UserProfile extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'user_id', 'character_id', 'nickname', 'firstname',
                                'lastname', 'avatar', 'vote_points', 'game_coins');

    /**
     * Retorna o Usuario pertencente a este Profile
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Informa de o usuário tem pontos suficientes para comprar x
     *
     * @param $for
     * @return bool
     */
    public function has_vote_points($for)
    {
        if ($this->vote_points >= Config::get('server.cost.'. $for)) {
            return true;
        }
        return false;
    }

    /**
     * Informa de o usuário tem moedas suficientes para comprar x
     *
     * @param $for
     * @return bool
     */
    public function has_game_coins($for)
    {
        if ($this->game_coins >= Config::get('server.cost.'. $for)) {
            return true;
        }
        return false;
    }

    /**
     * Retorna o Personagem selecionado do usuário
     *
     * @return User
     */
    public function character()
    {
        try {
            return WowCharacter::where('guid', '=', $this->character_id)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return null;
        }
    }
}