<?php

class WowCharacter extends Eloquent {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'characters';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'characters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('guid', 'account', 'name', 'race', 'class', 'gender', 'level');

    /**
     * Lista das raças do jogo
     *
     * @var array
     */
    public static $races = array(
        1  => "Human",
        2  => "Orc",
        3  => "Dwarf",
        4  => "Night Elf",
        5  => "Undead",
        6  => "Tauren",
        7  => "Gnome",
        8  => "Troll",
        10 => "Blood Elf",
        11 => "Draenei",
    );

    /**
     * Lista das classes do jogo
     *
     * @var array
     */
    public static $classes = array(
        1  => "Warrior",
        2  => "Paladin",
        3  => "Hunter",
        4  => "Rogue",
        5  => "Priest",
        7  => "Shaman",
        8  => "Mage",
        9  => "Warlock",
        11 => "Druid",
    );

    /**
     * Lista uma string com o nome da raça do personagem
     *
     * @return string
     */
    public function get_str_race()
    {
        return WowCharacter::$races[$this->race];
    }

    /**
     * Lista uma string com o nome da classe do personagem
     *
     * @return string
     */
    public function get_str_class()
    {
        return WowCharacter::$classes[$this->class];
    }

    /**
     * Retorna uma string com a url do avatar
     *
     * @return string
     */
    public function get_url_avatar()
    {
        $avatar = "/images/avatar/". WowCharacter::$classes[$this->class] ."-";
        $avatar .= str_replace(" ", "", strtolower(WowCharacter::$races[$this->race])) ."-";
        $avatar .= ($this->gender) ? "f" : "m";

        if ($this->level < 10) {
            $avatar = "/images/avatar/races/". $this->race ."-". $this->gender .".jpg";
        }
        elseif ($this->level < 60) {
            $avatar .= "-1.gif";
        }
        elseif ($this->level < 70) {
            $avatar .= "-60.gif";
        }
        else {
            $avatar .= "-70.gif";
        }

        return $avatar;
    }

    /**
     * Retorna uma lista de amigos do personagem
     *
     * @return array
     */
    public function friends()
    {
        $friends = array();
        foreach (WowCharacterSocial::where('guid', '=', $this->guid)->get() as $character) {
            array_push($friends, $character->friend());
        }

        return $friends;
    }

    /**
     * Verifica a facção do personagem
     *
     * @return boolean
     */
    public function isHorde() {
        return (in_array($this->race, array(2, 5, 6, 8, 10)));
    }

    /**
     * Envia uma requisição de troca de nome do personagem
     *
     * @return string
     */
    public function change_name()
    {
        $return = array(
            'success' => false,
            'errors' => array("Falha ao tentar trocar o nome do personagem.")
        );

        try {
            $client = ServerController::SoapClient();
            $result = $client->executeCommand(new SoapParam("character rename ". $this->name, 'command'));

            if (strpos($result, 'Forced rename') !== false) {
                $return['success'] = true;
                $return['errors'] = array();
            }
        }
        catch(SoapFault $e) {
            //
        }

        return $return;
    }

    /**
     * Retorna o total de jogadores online
     *
     * @return array
     */
    public static function total_online()
    {
        return WowCharacter::where('online', '=', 1)->count();
    }

}