<?php

class WowCharacterSocial extends Eloquent {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'characters';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'character_social';

    public function friend()
    {
        return WowCharacter::where('guid', '=', $this->friend)->firstOrFail();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('guid', 'friend');

}