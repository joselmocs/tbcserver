<?php

class User extends Toddish\Verify\Models\User {

    /**
     * Database associated with the model
     *
     * @var string
     */
    public $connection = 'website';

    /**
     * Retorna o Account do usuário no jogo
     *
     * @return WowAccount
     */
    public function account()
    {
        return $this->hasOne('WowAccount', 'user_id');
    }

    /**
     * Retorna todas as noticias que o usuario ja postou.
     *
     * @return array|Blog
     */
    public function blogs()
    {
        return $this->hasMany('Blog', 'user_id');
    }

    /**
     * Retorna todas os comentários que o usuario ja postou.
     *
     * @return array|BlogReply
     */
    public function replys()
    {
        return $this->hasMany('BlogReply', 'user_id');
    }

    public function change_password($password)
    {
        // Altera a senha do usuário no servidor
        $this->account->change_password($password);

        // Altera a senha do usuário no site
        $this->password = $password;
        $this->save();
    }

    /**
     * Retorna o profile do usuario, caso não existe é criado um profile novo.
     *
     * @return UserProfile
     */
    public function profile()
    {
        try {
            UserProfile::where('user_id', '=', $this->id)->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            UserProfile::create(array(
                'user_id' => $this->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }

        return $this->hasOne('UserProfile', 'user_id');
    }

    /**
     * Retorna o email do usuário com máscara escondendo parte do mesmo
     *
     * @return UserProfile
     */
    public function email_mask()
    {
        $mail_explode = explode("@", $this->email);
        $start_str = substr($mail_explode[0], 0, 3);
        $final_str = str_repeat("*", strlen(substr($mail_explode[0], 3, -1)));

        return strtolower($start_str . $final_str . substr($mail_explode[0], -1) ."@". $mail_explode[1]);
    }

}