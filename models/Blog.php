<?php

class Blog extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
     * Retorna o usuario que postou o blog.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna a quantidade de respostas que o blog tem.
     *
     * @return integer
     */
    public function reply()
    {
        return $this->hasMany('BlogReply', 'blog_id');
    }

    public function slug()
    {
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $this->title);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '_', $clean);

        return $clean;
    }
}