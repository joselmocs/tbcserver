<?php

class UserVote extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('id', 'user_id', 'votesite', 'updated_at');

    /**
     * Retorna o Usuario pertencente a este Profile
     *
     * @return User
     */
    public function user()
    {
        return $this->hasOne('User', 'user_id');
    }

    /**
     * Informa se o usuário pode votar ou não
     *
     * @return boolean
     */
    public function can_vote()
    {
        if (strtotime($this->updated_at) < $this->wait_time()) {
            return true;
        }
        return false;
    }

    /**
     * Retorna o time que o usuário precisa aguardar para poder votar
     *
     * @return string
     */
    public function wait_time()
    {
        return time() - 60 * 60 * Config::get('server.vote.wait_time');
    }

    /**
     * Retorna o tempo em string que o usuário precisa aguardar para poder votar
     *
     * @return string
     */
    public function str_wait_time()
    {
        $time = strtotime($this->updated_at) - $this->wait_time();

        $hora = $time / 60 / 60;
        if ($hora < 1) {
            $plural = (ceil($time / 60) > 1) ? 's' : '';
            return ceil($time / 60) . ' minuto'. $plural;
        }
        $plural = (floor($hora) > 1) ? 's' : '';

        return floor($hora) .' hora'. $plural;
    }

    /**
     * Retorna o registro de voto do usuário para o site x
     *
     * @param $votesite
     * @return UserVote
     */
    public static function from_site($votesite)
    {
        $query = UserVote::where('user_id', '=', Auth::user()->id)->where('votesite', '=', $votesite);
        try {
            $query->firstOrFail();
        }
        catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            UserVote::create(array(
                'user_id' => Auth::user()->id,
                'votesite' => $votesite,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ));
        }

        return $query->firstOrFail();
    }
}