<?php

class BlogReply extends Eloquent {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blogs_reply';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('user_id', 'character_id', 'blog_id', 'reply_to_id', 'text');

    /**
     * Retorna o usuario que postou a resposta.
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * Retorna o blog que a resposta pertence.
     *
     * @return User
     */
    public function blog()
    {
        return $this->belongsTo('Blog', 'blog_id');
    }

    /**
     * Retorna as respostas
     *
     * @return User
     */
    public function replys()
    {
        return $this->hasMany('BlogReply', 'reply_to_id');
    }

    /**
     * Retorna o character que fez o comentário
     *
     * @return WowCharacter
     */
    public function character()
    {
        return WowCharacter::where('guid', '=', $this->character_id)->firstOrFail();
    }
}