<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Server Routes
|--------------------------------------------------------------------------
*/

Route::get('server/features', 'ServerController@get_features');
Route::get('server/how-to-connect', 'ServerController@get_how_to_connect');

if (Request::ajax()) {
    Route::group(array('before' => 'csrf'), function() {
        Route::post('server/status', 'ServerController@post_status');
    });
}


/*
|--------------------------------------------------------------------------
| Blog Routes
|--------------------------------------------------------------------------
*/

Route::any('/', 'BlogController@any_index');
Route::get('blog/{id}/{slug}', 'BlogController@get_blog');

Route::group(array('before' => 'auth'), function()
{
    Route::group(array('before' => 'csrf'), function()
    {
        Route::post('blog/reply/add', 'BlogController@post_reply_add');

        if (Request::ajax()) {
            Route::post('blog/reply/delete', 'BlogController@post_reply_delete');
        }
    });
});

/*
|--------------------------------------------------------------------------
| Vote sites Routes
|--------------------------------------------------------------------------
*/
Route::get('/vote/{votesite}', 'VoteController@get_index');


/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/
Route::group(array('before' => 'auth'), function()
{
    Route::get('dashboard/{mode?}', 'DashboardController@get_dashboard');

    Route::group(array('before' => 'csrf'), function()
    {
        Route::post('dashboard/profile/change', 'DashboardController@post_profile_change');
        if (Request::ajax()) {
            Route::post('dashboard/change-name', 'DashboardController@post_change_name');
            Route::post('dashboard/password/change', 'DashboardController@post_password_change');
        }
    });

});

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
*/

Route::get('user/retrieve', 'UserController@get_retrieve');
Route::get('user/retrieve/{user}/{salt}', 'UserController@get_retrieve_password');
Route::get('user/register', 'UserController@get_register');
Route::get('user/activate/{user}/{salt}', 'UserController@get_activate');

Route::group(array('before' => 'auth'), function()
{
    Route::get('user/logout', 'UserController@get_logout');
});

Route::group(array('before' => 'csrf'), function()
{
    Route::post('user/login', 'UserController@post_login');

    if (Request::ajax()) {
        Route::post('user/retrieve', 'UserController@post_retrieve');
        Route::post('user/retrieve/password', 'UserController@post_retrieve_password');
        Route::post('user/register', 'UserController@post_register');
        Route::post('user/password/change', 'UserController@post_password_change');
    }
});
